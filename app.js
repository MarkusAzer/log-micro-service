/* eslint-disable no-console */
const cors = require('cors');
const morgan = require('morgan');
const empty = require('is-empty');
const express = require('express');
const mongoose = require('mongoose');
const bluebird = require('bluebird');
const { hidePoweredBy } = require('helmet');
const { serve, setup } = require('swagger-ui-express');

const config = require('./config/config');
// Expose config globally
global.ServerConfig = config;

const { PORT, MONGODB_URI, WHITE_LIST, NOTIFICATIONS } = config;
const swaggerSpec = require('./swagger');
const { catchAsyncErrors } = require('./middlewares/error');
const { errorHandler, dataHandler } = require('./middlewares/responseWrapper');
const { clientError, notification, validate, requiredParam } = require('./utilities');

const corsOptions = {
  origin: function(origin, callback) { // eslint-disable-line
    if (WHITE_LIST.indexOf(origin) !== -1 || !origin) {
      callback(null, true);
    } else {
      callback(new ClientError('Not allowed', 400));
    }
  },
};

// Set Global Promise & Empty & Validate & CatchAsyncErrors & ClientError & RequiredParam & SendNotifications
global.Promise = bluebird;
global.Empty = empty;
global.Validate = validate;
global.CatchAsyncErrors = catchAsyncErrors;
global.ClientError = clientError;
global.RequiredParam = requiredParam;
global.SendNotifications = notification({ notifications: NOTIFICATIONS });

// Configure mongoose
mongoose.Promise = bluebird;
mongoose.connect(MONGODB_URI, { useNewUrlParser: true, useCreateIndex: true, useUnifiedTopology: true }).catch(err => {
  console.log(`MongoDB connection error. Please make sure MongoDB is running. ${err}`);
  process.exit();
});

global.Models = require('./models'); // Push Models Once to global

// To Print Unhandled Error
process.on(
  'unhandledRejection',
  /** @type {function({ message: string, stack: any }): void} */
  error => {
    SendNotifications.sendSlackNotification('', `Error on log microservice api \n \`\`\`${error.stack}\`\`\``).catch(console.log);
    console.error('unhandledRejection', error.message);
  }
);

// Create new express app and get the port
const app = express();

// Swagger
app.use('/docs', serve, setup(swaggerSpec));

// Configure the app Middlewares
app.use(hidePoweredBy({ setTo: 'PHP/5.4.0' }));
app.use(express.json());
app.use(cors(corsOptions));
app.use(express.urlencoded({ extended: true }));
app.use(morgan('tiny'));

// Import Routes
const indexRoute = require('./routes');
const userRoute = require('./routes/user');
const logRoute = require('./routes/log');

// Route Middlewares
app.use('/', indexRoute);
app.use('/user', userRoute);
app.use('/log', logRoute);

// The Catch all Not found route
app.all('*', (req, res, next) => next(new ClientError('Not Found', 404)));

// Error handler
app.use(errorHandler);

// Data handler
app.use(dataHandler);

// Bind the app to the port
app.listen(PORT, () => console.log(`Server Up and Running \n=> http://localhost:${PORT}`));

module.exports = app;
