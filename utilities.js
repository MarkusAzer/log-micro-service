const https = require('https');
const Joi = require('@hapi/joi');
const crypto = require('crypto');
const nodemailer = require('nodemailer');

const { MAILSERVICE, HOST } = ServerConfig;

class RequiredParameterError extends Error {
  constructor(param) {
    super(`${param} can not be null or underfined.`);

    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, RequiredParameterError);
    }
  }
}

const cleanObject = obj =>
  Object.keys(obj)
    .filter(k => !Empty(obj[k])) // Remove undef. and null.
    .reduce(
      (newObj, k) =>
        // eslint-disable-next-line no-nested-ternary
        Array.isArray(obj[k])
          ? { ...newObj, [k]: obj[k] }
          : typeof obj[k] === 'object'
          ? { ...newObj, [k]: cleanObject(obj[k]) } // Recurse.
          : { ...newObj, [k]: obj[k] }, // Copy value.
      {}
    );

module.exports = {
  cleanObject,
  // eslint-disable-next-line
  requiredParam: function(param) {
    throw new RequiredParameterError(param);
  },
  // eslint-disable-next-line
  clientError: function(message, errorCode) {
    Error.captureStackTrace(this, this.constructor);

    this.name = this.constructor.name;
    this.message = message || "The requested resource couldn't be found";
    this.errorCode = errorCode || 404;
  },
  validate: (type = RequiredParam('type'), schema = RequiredParam('schema')) => {
    return (req, res, next) => {
      // @ts-ignore
      const result = Joi.validate(req[type.toLowerCase()], schema, { abortEarly: false });
      if (result.error) next(result.error);

      req[type] = result.value;
      return next();
    };
  },
  generateRandomToken: async number => {
    const buffer = await new Promise((resolve, reject) => {
      crypto.randomBytes(number, (ex, buff) => {
        if (ex) {
          reject(new Error('Error generating token'));
        }
        resolve(buff);
      });
    });

    return crypto
      .createHash('sha1')
      .update(buffer)
      .digest('hex');
  },
  sendMail: async (mailOptions, callback) => {
    const { service, user, pass } = MAILSERVICE;
    const transporter = nodemailer.createTransport({ service, auth: { user, pass } });

    return transporter.sendMail({ from: user, ...mailOptions }, callback);
  },
  notification: config => {
    return {
      // eslint-disable-next-line arrow-body-style
      sendSlackNotification: (channelId, message) => {
        return new Promise((resolve, reject) => {
          try {
            const notificationsConfig = config && config.notifications;

            if (Empty(notificationsConfig)) {
              throw new Error('Notification Config is Missing');
            }

            const { slackHostName, slackToken } = notificationsConfig;
            if (Empty(slackHostName) || Empty(slackToken)) {
              throw new Error('Slack Host Name & Slack Token is required');
            }

            if (Empty(channelId) || Empty(message)) {
              throw new Error('channelId Or message is Missing');
            }

            const options = {
              hostname: slackHostName,
              port: 443,
              path: `/api/chat.postMessage?token=${slackToken}&channel=${channelId}&username=elnotifier&text=${encodeURI(message)}`,
              method: 'POST',
            };

            const req = https.request(options, response => {
              // Continuously update stream with data
              let body = '';
              // eslint-disable-next-line no-return-assign
              response.on('data', d => (body += d));

              // Data reception is done, do whatever with it!
              response.on('end', () => resolve(body));
            });
            req.on('error', error => reject(error));
            req.end();
          } catch (error) {
            reject(error);
          }
        });
      },
    };
  },
  emailTemplates: {
    verifyEmail: token => {
      return `
      <!DOCTYPE html>
      <html>
      <head>
        <title>Email Verification</title>
      </head>
      <body>
        <div>
          <h3>Hello</h3>
          <p>Please verify your email by clicking the link: <a href="${HOST}/user/reset-password?token=${token}">link</a></p>
          <br>
          <p>Cheers!</p>
        </div>
      </body>
      </html> `;
    },
  },
};
