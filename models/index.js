const User = require('./schemas/users');
const Token = require('./schemas/tokens');
const RestrictedToken = require('./schemas/restrictedTokens');
const Log = require('./schemas/logs');

// DataBase Layer
module.exports = {
  User: {
    validatePassword: (password, hashedPassword) => User.validatePassword(password, hashedPassword),
    countDocuments: query => User.countDocuments(query),
    findOne: (query, options) => {
      const defaultOptions = { lean: false, selectedFields: '' };
      /** @type {{ lean: boolean, selectedFields: string}} */
      const { lean, selectedFields } = { ...defaultOptions, ...options };

      return User.findOne(query, selectedFields).lean(lean);
    },
    create: data => User.create(data),
  },
  Token: {
    countDocuments: query => Token.countDocuments(query),
    findOne: (query, options) => {
      const defaultOptions = { lean: false, selectedFields: '' };
      /** @type {{ lean: boolean, selectedFields: string}} */
      const { lean, selectedFields } = { ...defaultOptions, ...options };

      return Token.findOne(query, selectedFields).lean(lean);
    },
    create: data => Token.create(data),
  },
  RestrictedToken: {
    countDocuments: query => RestrictedToken.countDocuments(query),
    findOne: (query, options) => {
      const defaultOptions = { lean: false, selectedFields: '' };
      /** @type {{ lean: boolean, selectedFields: string}} */
      const { lean, selectedFields } = { ...defaultOptions, ...options };

      return RestrictedToken.findOne(query, selectedFields).lean(lean);
    },
    create: data => RestrictedToken.create(data),
  },
  Log: {
    countDocuments: query => Log.countDocuments(query),
    findOne: (query, options) => {
      const defaultOptions = { lean: false, selectedFields: '' };
      /** @type {{ lean: boolean, selectedFields: string}} */
      const { lean, selectedFields } = { ...defaultOptions, ...options };

      return Log.findOne(query, selectedFields).lean(lean);
    },
    find: (query, options) => {
      const defaultOptions = { lean: false, selectedFields: '', limit: 30, page: 1 };
      /** @type {{ lean: boolean, selectedFields: string, limit: number, page: number }} */
      const { lean, selectedFields, limit, page } = { ...defaultOptions, ...options };

      return Log.find(query, selectedFields)
        .sort({ counter: -1 })
        .skip((page - 1) * limit)
        .limit(limit)
        .lean(lean);
    },
    create: data => Log.create(data),
  },
};
