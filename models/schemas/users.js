const mongoose = require('mongoose');
const bcrypt = require('bcrypt');

const { Schema } = mongoose;
const { checkModelFields, checkLockedFields, asyncIdGenerator } = require('../utilities');

const UserSchema = new Schema(
  {
    id: { type: String },
    email: { type: String, lowercase: true, required: true, unique: true },
    password: { type: String, required: true },
    name: { type: String, required: true },
    verifiedEmail: { type: Boolean, default: false },
    verified: { type: Boolean, default: false },
  },
  {
    timestamps: true,
  }
);

// eslint-disable-next-line func-names
UserSchema.static('validatePassword', function(password, hashedPassword) {
  return bcrypt.compare(password, hashedPassword);
});

// eslint-disable-next-line func-names
UserSchema.pre('save', async function(next) {
  try {
    checkModelFields(this, ['id', 'verified']);

    if (this.isNew) {
      this.id = await asyncIdGenerator.generateUniqueId(this, 'U', 12);
    }

    if (this.isModified('email') && this.verifiedEmail === true) {
      this.verifiedEmail = false;
      this.verified = false;
    }

    if (this.isModified('verifiedEmail') && this.verifiedEmail === true) {
      this.verified = true;
    }

    if (this.isModified('password')) {
      const clonePassword = JSON.parse(JSON.stringify(this.password));
      const salt = await bcrypt.genSalt(10);
      this.password = await bcrypt.hash(clonePassword, salt);
    }

    if (this.isModified('name')) this.nameModified = true;

    checkLockedFields(this, ['id', 'email']);
    return next();
  } catch (err) {
    return next(err);
  }
});

module.exports = mongoose.model('User', UserSchema, 'users');
