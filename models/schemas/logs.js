const mongoose = require('mongoose');

const { Schema } = mongoose;

const LogSchema = new Schema(
  {
    title: { type: String, required: true },
    description: { type: String },
    statusCode: { type: Number, index: true, required: true },
    path: { type: String, index: true, required: true },
    createdBy: { type: String, index: true, required: true },
    logCreatedDate: { type: Date, required: true },
  },
  { timestamps: true }
);

LogSchema.virtual('user', {
  ref: 'User',
  localField: 'createdBy',
  foreignField: 'id',
  justOne: true,
});

module.exports = mongoose.model('Log', LogSchema, 'logs');
