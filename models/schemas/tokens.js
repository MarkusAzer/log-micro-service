const mongoose = require('mongoose');

const { Schema, Types } = mongoose;
const { ObjectId } = Types;
const { foundValue } = require('../validators');

const TokenSchema = new Schema({
  user: {
    type: ObjectId,
    required: true,
    ref: 'User',
    validate: foundValue('User', '_id', 'user'),
  },
  type: { type: String, enum: ['verifyEmail'], required: true },
  token: { type: String, required: true },
  createdAt: { type: Date, required: true, default: Date.now, expires: 43200 }, // expires in 12 hours
});

module.exports = mongoose.model('Token', TokenSchema, 'tokens');
