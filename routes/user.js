// @ts-check
const router = require('express').Router();
const passport = require('passport');
require('../middlewares/passport');

const passportJWT = passport.authenticate('jwt', { session: false, failWithError: true });

const schemas = require('../controllers/userValidation');
const { signUp, emailConfirmation, signIn, signOut } = require('../controllers/user');

router.route('/signup').post(Validate('Body', schemas.signUp), CatchAsyncErrors(signUp));

router.route('/email-confirmation').post(Validate('Body', schemas.emailConfirmation), CatchAsyncErrors(emailConfirmation));

router.route('/signin').post(Validate('Body', schemas.signIn), passport.authenticate('local', { session: false }), CatchAsyncErrors(signIn));

router.route('/signout').get(passportJWT, CatchAsyncErrors(signOut));

module.exports = router;
