const router = require('express').Router();
const passport = require('passport');
require('../middlewares/passport');

const passportJWT = passport.authenticate('jwt', { session: false, failWithError: true });

const schemas = require('../controllers/logValidation');
const { find, create } = require('../controllers/log');

router
  .route('/')
  .all(passportJWT)
  .get(Validate('query', schemas.find), CatchAsyncErrors(find))
  .post(Validate('body', schemas.create), CatchAsyncErrors(create));

module.exports = router;
