// @ts-check
const router = require('express').Router();

router.route('/').get((req, res, next) => {
  return next({ message: 'Connected!' });
});

module.exports = router;
