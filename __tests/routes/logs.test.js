const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../../app');

const { expect } = chai;

chai.use(chaiHttp);

let token;

describe('Logs route', () => {
  const signin = '/user/signin';
  const logRoute = '/log';
  const user = {
    email: 'markusgihady@gmail.com',
    password: '123456',
  };

  before(async () => {
    const result = await chai
      .request(server)
      .post(signin)
      .send(user);
    expect(result.status).to.equal(200);
    token = result.body.data.token;
  });

  describe('create log', () => {
    it('should crete new log', async () => {
      try {
        const result = await chai
          .request(server)
          .post(logRoute)
          .set('Authorization', `Bearer ${token}`)
          .send({
            title: 'GET',
            description: 'New Log',
            statusCode: 200,
            path: '/log',
            logCreatedDate: '2019-10-21T17:30:00Z',
          });
        expect(result.status).to.equal(200);
        // eslint-disable-next-line no-unused-expressions
        expect(result.body).not.to.be.empty;
      } catch (error) {
        throw new Error(error);
      }
    });

    it('should return 401 if there is no bear token', async () => {
      try {
        await chai
          .request(server)
          .post(logRoute)
          .send({
            title: 'GET',
            description: 'New Log',
            statusCode: 200,
            path: '/log',
            logCreatedDate: '2019-10-21T17:30:00Z',
          });
      } catch (error) {
        expect(error.status).to.equal(401);
        expect(error.response.data.message).to.equal('Unauthorized');
      }
    });

    it('should return 400 if title is missing', async () => {
      try {
        await chai
          .request(server)
          .post(logRoute)
          .set('Authorization', `Bearer ${token}`)
          .send({
            description: 'New Log',
            statusCode: 200,
            path: '/log',
            logCreatedDate: '2019-10-21T17:30:00Z',
          });
      } catch (error) {
        expect(error.status).to.equal(400);
      }
    });
  });

  describe('find log', () => {
    it('should return logs', async () => {
      try {
        const result = await chai
          .request(server)
          .get(logRoute)
          .set('Authorization', `Bearer ${token}`);
        expect(result.status).to.equal(200);
        // eslint-disable-next-line no-unused-expressions
        expect(result.body).not.to.be.empty;
      } catch (error) {
        throw new Error(error);
      }
    });

    it('should return 401 if there is no bear token', async () => {
      try {
        await chai.request(server).get(logRoute);
      } catch (error) {
        expect(error.status).to.equal(401);
        expect(error.response.data.message).to.equal('Unauthorized');
      }
    });
  });
});
