// @ts-check
const passport = require('passport');
const JwtStrategy = require('passport-jwt').Strategy;
const { ExtractJwt } = require('passport-jwt');
const LocalStrategy = require('passport-local').Strategy;

const { User, RestrictedToken } = Models;
const config = ServerConfig;

// JSON WEB TOKENS STRATEGY
passport.use(
  new JwtStrategy(
    {
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      secretOrKey: config.JWT_SECRET_TOKEN,
      passReqToCallback: true,
    },
    async (req, payload, done) => {
      try {
        const { id } = payload;
        const bearerToken = req.headers.authorization;
        const token = bearerToken.replace('Bearer ', '');

        // TODO: Scale redis and refersh token
        const restrictedTokenPromise = RestrictedToken.findOne({ token });
        const userPromise = User.findOne({ id }, { lean: true });

        const [user, restrictedToken] = await Promise.all([userPromise, restrictedTokenPromise]);

        if (!user || restrictedToken) return done(undefined, false);
        if (!user.verified) throw new ClientError('Please Verify your email');

        return done(undefined, user, payload);
      } catch (error) {
        return done(error, false);
      }
    }
  )
);

// LOCAL STRATEGY
passport.use(
  new LocalStrategy(
    {
      usernameField: 'email',
    },
    async (email, password, done) => {
      try {
        const user = await User.findOne({ email }, { lean: true });

        if (!user) throw new ClientError(`Email ${email} not found.`, 401);

        const validPass = await User.validatePassword(password, user.password);

        if (!validPass) throw new ClientError('Invalid email or password.', 400);
        if (!user.verified) throw new ClientError('Please Verify your email', 403);

        return done(undefined, user);
      } catch (error) {
        return done(error);
      }
    }
  )
);
