// @ts-check
const { isDev, SLACK_ERROR_CHANNEL } = ServerConfig;

module.exports = {
  /** @type {function({ message: string, errorCode: number, name?: string, isJoi?: boolean, details?: any }, any, any, any): void} */
  errorHandler: (payload, req, res, next) => {
    try {
      if (payload instanceof ClientError) {
        return res.status(payload.errorCode).json({
          status: payload.errorCode,
          message: payload.message,
          successful: false,
        });
      }

      if (payload instanceof SyntaxError) {
        return res.status(400).json({
          status: 400,
          message: `Invalid JSON`,
          successful: false,
        });
      }

      if (payload.name === 'AuthenticationError') {
        return res.status(401).json({
          status: 401,
          message: `Unauthorized`,
          successful: false,
        });
      }

      if (payload.name === 'ValidationError' && payload.isJoi === true) {
        const validationsArray = payload.details.map(detail => detail.message.replace(/"+/g, ''));

        return res.status(payload.errorCode || 400).json({
          status: payload.errorCode || 400,
          message: 'Validation Errors',
          validations: validationsArray,
          successful: false,
        });
      }

      if (payload instanceof Error) {
        if (isDev) {
          // eslint-disable-next-line no-console
          console.log(`DEVELOPMENT ERRORS \n ${payload}`);
        }

        // eslint-disable-next-line no-undef
        SendNotifications.sendSlackNotification(SLACK_ERROR_CHANNEL, `Error on log microservice api \n \`\`\`${payload.stack}\`\`\``).catch(
          console.log // eslint-disable-line no-console
        );

        return res.status(500).json({
          status: 500,
          message: 'Internal Server Error',
          successful: false,
        });
      }

      return next(payload);
    } catch (error) {
      // eslint-disable-next-line no-console
      console.log(error);
      return res.status(500).json({
        status: 500,
        message: 'Internal Server Error',
        successful: false,
      });
    }
  },
  // eslint-disable-next-line no-unused-vars
  dataHandler: (payload, req, res, next) =>
    res.status(200).json({
      status: 200,
      message: payload.message ? payload.message : 'data successfully retrieved',
      data: payload && payload.data,
      successful: true,
    }),
};
