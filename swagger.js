const swaggerJSDoc = require('swagger-jsdoc');

const { HOST } = ServerConfig;
// swagger definition
const swaggerDefinition = {
  openapi: '3.0.1',
  info: {
    title: 'Log Microservice API',
    version: '1.0.0',
    description: 'Log Microservice API Doc',
  },
  host: HOST,
  servers: [],
  components: {
    securitySchemes: {
      bearerAuth: {
        type: 'http',
        scheme: 'bearer',
        bearerFormat: 'JWT',
      },
    },
  },
};

// options for the swagger docs
const options = {
  // import swaggerDefinitions
  swaggerDefinition,
  // path to the API docs
  apis: ['./routes/**/*.yaml'], // pass all in array
};

// initialize swagger-jsdoc
module.exports = swaggerJSDoc(options);
