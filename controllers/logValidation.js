const Joi = require('@hapi/joi');

module.exports = {
  create: Joi.alternatives()
    .try(
      Joi.object().keys({
        title: Joi.string().required(),
        description: Joi.string(),
        statusCode: Joi.number().required(),
        path: Joi.string().required(),
        logCreatedDate: Joi.date()
          .iso()
          .required(),
      }),
      Joi.array()
        .items({
          title: Joi.string().required(),
          description: Joi.string(),
          statusCode: Joi.number().required(),
          path: Joi.string().required(),
          logCreatedDate: Joi.date()
            .iso()
            .required(),
        })
        .length(2)
    )
    .required(),
  find: Joi.object().keys({
    statusCode: Joi.number(),
    path: Joi.string(),
    createdBy: Joi.string(),
    page: Joi.number(),
    limit: Joi.number(),
  }),
};
