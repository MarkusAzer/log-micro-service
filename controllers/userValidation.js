// @ts-check
const Joi = require('@hapi/joi');

module.exports = {
  signUp: Joi.object().keys({
    name: Joi.string()
      .min(3)
      .max(40)
      .required(),
    email: Joi.string()
      .email({ minDomainSegments: 2 })
      .required(),
    password: Joi.string()
      .min(6)
      .max(30)
      .required(),
    confirmPassword: Joi.string()
      .valid(Joi.ref('password'))
      .required()
      .strict()
      .strip(),
  }),
  emailConfirmation: Joi.object().keys({
    email: Joi.string()
      .email({ minDomainSegments: 2 })
      .required(),
    token: Joi.string().required(),
  }),
  signIn: Joi.object().keys({
    email: Joi.string()
      .email({ minDomainSegments: 2 })
      .required(),
    password: Joi.string()
      .min(6)
      .max(30)
      .required(),
  }),
};
