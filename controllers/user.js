const jwt = require('jsonwebtoken');
const { generateRandomToken, emailTemplates, sendMail } = require('../utilities');

const { User, Token, RestrictedToken } = Models;
const { JWT_SECRET_TOKEN } = ServerConfig;

/** Helpers */
const signToken = ({ id }) => jwt.sign({ id }, JWT_SECRET_TOKEN, { expiresIn: 2592000 });

module.exports = {
  signUp: async (req, res, next) => {
    const { name, password, email } = req.body;

    const emailDuplicationPromise = User.countDocuments({ email });
    const emailVerificationTokenPromise = generateRandomToken(20);
    const [emailDuplication, emailVerificationToken] = await Promise.all([emailDuplicationPromise, emailVerificationTokenPromise]);

    if (emailDuplication !== 0) throw new ClientError('Account with that email address already exists.', 400);

    const user = await User.create({ email, password, name });

    Token.create({
      user: user._id,
      type: 'verifyEmail',
      token: emailVerificationToken,
    });

    const html = emailTemplates.verifyEmail(emailVerificationToken);

    sendMail({ to: email, subject: 'Email Verification', html }, err => {
      if (err) return next(err);

      return next({ message: 'Sign Up Successfully, Please Verify your email' });
    });
  },
  emailConfirmation: async (req, res, next) => {
    const { email, token } = req.body;

    const emailVerificationToken = await Token.findOne({ token, type: 'verifyEmail' });
    // TODO: handle the newest token
    if (Empty(emailVerificationToken)) throw new ClientError(`Not valid token`, 400);

    const user = await User.findOne({ _id: emailVerificationToken.user, email });
    if (Empty(user)) throw new ClientError(`Not valid token`, 400);
    await emailVerificationToken.remove();
    if (user.verifiedEmail) throw new ClientError(`This user email has already been verified`, 400);

    user.verifiedEmail = true;
    await user.save();

    return next({ message: 'Account has been verified' });
  },
  signIn: async (req, res, next) => {
    const { id } = req.user;

    return next({ data: { token: signToken({ id }), user: { id } } });
  },
  signOut: async (req, res, next) => {
    const token = req.headers.authorization.replace('Bearer ', '');
    RestrictedToken.create({ token });
    return next({ message: 'Sign Out Successfully' });
  },
};
