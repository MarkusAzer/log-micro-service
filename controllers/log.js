const { cleanObject } = require('../utilities');

const { Log } = Models;

module.exports = {
  find: async (req, res, next) => {
    const { page, limit, statusCode, path, createdBy } = req.query;
    const pageOptions = { page: parseInt(page, 10) || 1, limit: parseInt(limit, 10) || 30 };
    const query = cleanObject({ statusCode, path, createdBy });

    const logsPromise = Log.find(query, {
      selectedFields: '-_id title description statusCode path logCreatedDate createdBy',
      limit: pageOptions.limit,
      page: pageOptions.page,
    });

    const logsTotalPromise = Log.countDocuments(query);

    const [logs, logsTotal] = await Promise.all([logsPromise, logsTotalPromise]);

    return next({ data: { logs, total: logsTotal, limit: pageOptions.limit, page: pageOptions.page } });
  },
  create: async (req, res, next) => {
    const data = req.body;
    const { id } = req.user;

    const logs = Array.isArray(data) ? data.map(d => ({ ...d, createdBy: id })) : { ...data, createdBy: id };

    await Log.create(logs);

    return next({ message: 'Log Created Successfully' });
  },
};
