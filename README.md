# Getting started
- Clone the repository using SSH
```
git clone git@gitlab.com:MarkusAzer/log-micro-service.git
```
- Navigate to project
```
cd log-micro-service
```
- Copy config/env.sample.js file into a file called config/env.js and replace configuration with your own.
```
cp config/env.sample.js config/env.js
```
- Build and run the project
```
docker-compose up

```

- Swagger
```
{{url}}/docs
```
