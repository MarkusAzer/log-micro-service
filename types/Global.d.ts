/* eslint-disable */
type empty = (value: any) => boolean;
type clientError = (message: string, errorCode?: number) => void;
type models = {
    User: any,
    Token: any,
    RestrictedToken: any,
    Log: any,
};
type serverConfig = {
    JWT_SECRET_TOKEN: string,
    SLACK_ERROR_CHANNEL: string,
    isDev: boolean,
    HOST: string,
    MAILSERVICE: {
      service: string,
      user: string,
      pass: string,
    }
};
type sendNotifications = {
  sendSlackNotification: (slackChannelError: string, message: string) => Promise<void>;
}
type validate = (type: string, schema: any) => any;
type catchAsyncErrors = any;
type requiredParam = (param: string) => any;

declare module NodeJS {
  interface Global {
    Empty: empty;
    ClientError: clientError;
    Models: models;
    ServerConfig: serverConfig;
    SendNotifications: sendNotifications;
    Validate: validate;
    CatchAsyncErrors: catchAsyncErrors;
    RequiredParam: requiredParam;
  }
}

declare const Empty: empty;
declare const ClientError: clientError;
declare const Models: models;
declare const ServerConfig: serverConfig;
declare const SendNotifications: sendNotifications;
declare const Validate: validate;
declare const CatchAsyncErrors: catchAsyncErrors;
declare const RequiredParam: requiredParam;
